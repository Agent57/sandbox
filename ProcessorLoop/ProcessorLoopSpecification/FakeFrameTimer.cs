﻿using System.Collections.Generic;
using ProcessorLoop;

namespace ProcessorLoopSpecification
{
  class FakeFrameTimer : IFrameTimer
  {
    public int Lag { get; set; }
    public int PreviousTime { get; set; }
    public Queue<int> LagTimes { get; set; }

    public FakeFrameTimer()
    {
      Lag = 0;
      PreviousTime = 0;
      LagTimes = new Queue<int>();
    }

    public int GetTime()
    {
      if (Lag > 16)
      {
        Lag -= 16;
      }
      else
      {
        Lag = LagTimes.Count > 0 ? LagTimes.Dequeue() : 0;
      }

      return Lag;
    }

    public bool Lagging
    {
      get
      {
        return GetTime() > 16;
      }
    }
  }
}
