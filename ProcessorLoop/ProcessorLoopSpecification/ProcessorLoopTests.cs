﻿using NUnit.Framework;
using ProcessorLoop;

namespace ProcessorLoopSpecification
{
  [TestFixture]
  public class ProcessorLoopTests
  {
    [Test]
    public void It_does_nothing_when_the_loop_is_stopped()
    {
      var processor = new TestProcessor {LoopsToRunFor = 0};

      var loop = new ProcessorLoop.ProcessorLoop(processor, new NullInputHandler(), new NullFrameTimer());
      loop.Run();

      Assert.IsFalse(processor.Updated);
    }

    [Test]
    public void It_calls_update_once_before_the_loop_is_stopped()
    {
      var processor = new TestProcessor {LoopsToRunFor = 1};

      var loop = new ProcessorLoop.ProcessorLoop(processor, new NullInputHandler(), new NullFrameTimer());

      loop.Run();

      Assert.IsTrue(processor.Updated);
    }

    [Test]
    public void It_runs_update_until_the_loop_is_stopped()
    {
      var processor = new TestProcessor {LoopsToRunFor = 2};

      var loop = new ProcessorLoop.ProcessorLoop(processor, new NullInputHandler(), new NullFrameTimer());

      loop.Run();

      Assert.AreEqual(2, processor.UpdateCount);
    }

    [Test]
    public void It_does_a_draw_after_update()
    {
      var processor = new TestProcessor {LoopsToRunFor = 1};

      var loop = new ProcessorLoop.ProcessorLoop(processor, new NullInputHandler(), new NullFrameTimer());

      loop.Run();

      Assert.AreEqual(1, processor.DrawCount);
    }

    [Test]
    public void It_passes_the_input_results_to_update()
    {
      var processor = new TestProcessor {LoopsToRunFor = 1};
      var handler = new FakeInputHandler();

      var loop = new ProcessorLoop.ProcessorLoop(processor, handler, new NullFrameTimer());
      loop.Run();

      Assert.AreSame(handler.ReturnedInput, processor.UpdatedWith);
    }

    [Test]
    public void It_will_keep_calling_update_to_catch_up_when_the_lag_is_too_large()
    {
      var processor = new TestProcessor {LoopsToRunFor = 1};
      var timer = new FakeFrameTimer();
      timer.LagTimes.Enqueue(17);

      var loop = new ProcessorLoop.ProcessorLoop(processor, new NullInputHandler(), timer);

      loop.Run();

      Assert.AreEqual(2, processor.UpdateCount);
      Assert.AreEqual(1, processor.DrawCount);
    }

    [Test]
    public void It_will_poll_input_once_in_each_loop_before_updating()
    {
      var processor = new TestProcessor {LoopsToRunFor = 2};
      var handler = new FakeInputHandler {ReturnedInput = new FakeInputState()};
      var timer = new FakeFrameTimer();
      timer.LagTimes.Enqueue(34);

      var loop = new ProcessorLoop.ProcessorLoop(processor, handler, timer);

      loop.Run();

      Assert.AreEqual(4, processor.UpdateCount);
      Assert.AreEqual(2, ((FakeInputState)handler.ReturnedInput).PollCount);
    }
  }
}
