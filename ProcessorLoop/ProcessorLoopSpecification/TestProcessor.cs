﻿using ProcessorLoop;

namespace ProcessorLoopSpecification
{
  public class TestProcessor : IProcessor
  {
    public int UpdateCount { get; set; }
    public int DrawCount { get; set; }
    public IInputState UpdatedWith { get; set; }
    public int LoopsToRunFor { get; set; }

    public bool Updated { get { return UpdateCount > 0; } }

    public bool Running
    {
      get
      {
        LoopsToRunFor--;
        return LoopsToRunFor >= 0;
      }
    }

    public TestProcessor()
    {
      UpdateCount = 0;
      DrawCount = 0;
      LoopsToRunFor = 0;
    }

    public void Update(IInputState input)
    {
      var inputState = input as FakeInputState;
      if (inputState != null)
      {
        if(inputState.PollCount != DrawCount + 1)
          throw new InvalidInputException("Poll() has not been called correctly");
      }
      
      UpdateCount++;
      UpdatedWith = input;
    }

    public void Draw()
    {
      if(DrawCount >= UpdateCount)
        throw new InvalidDrawException("Draw() was called before Update()");
      
      DrawCount++;
    }
  }
}
