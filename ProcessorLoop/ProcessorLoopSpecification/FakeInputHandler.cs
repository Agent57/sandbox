﻿using ProcessorLoop;

namespace ProcessorLoopSpecification
{
  class FakeInputHandler : IInputHandler
  {
    public IInputState ReturnedInput { get; set; }

    public FakeInputHandler()
    {
      ReturnedInput = new NullInputState();
    }

    public IInputState CurrentState
    {
      get
      {
        return ReturnedInput;
      }
    }

    public void Poll()
    {
      var inputState = ReturnedInput as FakeInputState;
      if (inputState != null)
        inputState.PollCount++;
    }
  }
}
