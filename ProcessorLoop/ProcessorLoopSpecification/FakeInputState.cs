﻿using ProcessorLoop;

namespace ProcessorLoopSpecification
{
  public class FakeInputState : IInputState
  {
    public int PollCount { get; set; }

    public FakeInputState()
    {
      PollCount = 0;
    }
  }
}
