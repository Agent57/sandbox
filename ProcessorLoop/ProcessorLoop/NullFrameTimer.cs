﻿namespace ProcessorLoop
{
  public class NullFrameTimer : IFrameTimer
  {
    public int GetTime() { return 0; }
    public bool Lagging { get { return false; } }
  }
}
