﻿namespace ProcessorLoop
{
  public interface IFrameTimer
  {
    int GetTime();

    bool Lagging { get; }
  }
}
