﻿namespace ProcessorLoop
{
  public class NullInputHandler : IInputHandler
  {
    public IInputState CurrentState { get { return new NullInputState(); } }
    public void Poll() { }
  }
}
