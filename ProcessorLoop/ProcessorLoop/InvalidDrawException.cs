﻿using System;

namespace ProcessorLoop
{
  public class InvalidDrawException : Exception
  {
    public InvalidDrawException() { }
    public InvalidDrawException(string message) : base(message) { }
  }
}
