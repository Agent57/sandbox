﻿namespace ProcessorLoop
{
  public class ProcessorLoop : IProcessorLoop
  {
    private readonly IProcessor _processor;
    private readonly IInputHandler _inputHandler;
    private readonly IFrameTimer _frameTimer;

    public ProcessorLoop(IProcessor processor, IInputHandler handler, IFrameTimer timer)
    {
      _processor = processor;
      _inputHandler = handler;
      _frameTimer = timer;
    }

    public void Run()
    {
      while (_processor.Running)
      {
        _inputHandler.Poll();
        do
        {
          _processor.Update(_inputHandler.CurrentState);
        } while (_frameTimer.Lagging);
        _processor.Draw();
      }
    }
  }
}
