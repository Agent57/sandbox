﻿namespace ProcessorLoop
{
  public interface IInputHandler
  {
    IInputState CurrentState { get; }
    void Poll();
  }
}
