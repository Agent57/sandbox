﻿using System;

namespace ProcessorLoop
{
  public class InvalidInputException : Exception
  {
    public InvalidInputException() { }
    public InvalidInputException(string message) : base(message) { }
  }
}
