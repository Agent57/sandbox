﻿namespace ProcessorLoop
{
  public interface IProcessor
  {
    bool Running { get; }
    void Update(IInputState input);
    void Draw();
  }
}
