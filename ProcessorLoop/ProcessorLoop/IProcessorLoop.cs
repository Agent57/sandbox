﻿namespace ProcessorLoop
{
  public interface IProcessorLoop
  {
    void Run();
  }
}
