#include "SocketContext.h"

void SocketContext::Open(const std::string& host, int port)
{
  m_state->Open(host, port);
}

void SocketContext::Listen(int port) 
{
  m_state->Listen(port);
}

SocketContextPtr SocketContext::Accept()
{
  return m_state->Accept();
}

int SocketContext::Read(char* buffer, int length)
{
  return m_state->Read(buffer, length);
}

int SocketContext::Write(char* buffer, int length)
{
  return m_state->Write(buffer, length);
}

void SocketContext::Close()
{
  m_state->Close();
}

void SocketContext::SetState(SocketActions state)
{
  m_state = move(state);
}
