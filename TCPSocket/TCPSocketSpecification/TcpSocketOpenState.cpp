#include "TcpSocketOpenState.h"
#include "TcpSocketClosedState.h"

TcpSocketOpenState::~TcpSocketOpenState()
{
  if (m_handle)
    m_handle->Shutdown();
}

int TcpSocketOpenState::Read(char* buffer, int length)
{
  return m_handle->Receive(buffer, length);
}

int TcpSocketOpenState::Write(char* buffer, int length)
{
  return m_handle->Send(buffer, length);
}

void TcpSocketOpenState::Close()
{
  m_handle->Shutdown();
  if (m_context)
    m_context->SetState(std::make_unique<TcpSocketClosedState>(m_context, move(m_handle)));
}