#pragma once

#include "SocketState.h"

class TcpSocketListeningState : public SocketState
{
public:
  explicit TcpSocketListeningState(SocketContext* context, SocketHandlePtr handle)
    : SocketState(context, move(handle))
  {    
  }

  virtual ~TcpSocketListeningState();

  virtual SocketContextPtr Accept() override;
  virtual void Close() override;
};
