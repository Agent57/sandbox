#pragma once

#include "ISocketHandle.h"

#include <winsock2.h>

class TcpSocketHandle : public ISocketHandle
{
  SOCKET m_socket;
  struct addrinfo *m_info = nullptr;

public:
  TcpSocketHandle();
  explicit TcpSocketHandle(SOCKET handle);

  ~TcpSocketHandle() override;

  void Listen(int port) override;
  SocketHandlePtr Accept() override;
  void Connect(std::string const& host, int port) override;
  int Send(char* buffer, int length) override;
  int Receive(char* buffer, int length) override;
  void Shutdown() override;
  void Close() override;

private:
  void Initialise();
  void SetSocketInfo(char const* const host, int port);
  void CreateSocket(PADDRINFOA info);
};
