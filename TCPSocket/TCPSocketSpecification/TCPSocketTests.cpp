#include "CppUnitTest.h"

#include <thread>

#include "TcpSocketClosedState.h"
#include "TcpSocketHandle.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TCPSocketSpecification
{
  TEST_CLASS(UnitTest1)
  {
    char* TEST_MESSAGE = "Hello World";
    static const int TEST_MESSAGE_SIZE = 11;
    static const int TEST_BUFFER_SIZE = 256;
    static const int TEST_SOCKET_PORT = 1337;

    std::thread m_server;
    char m_receiveBuffer[TEST_BUFFER_SIZE];
    std::shared_ptr<SocketContext> server;

  public:
    TEST_METHOD(A_closed_tcp_socket_will_throw_a_runtime_error_when_trying_to_send)
    {
      try
      {
        auto client = std::make_shared<SocketContext>();
        client->Initialise<TcpSocketClosedState, TcpSocketHandle>();
        client->Write(TEST_MESSAGE, TEST_MESSAGE_SIZE);
        Assert::Fail(L"Runtime Error Expected");
      }
      catch (SocketException e)
      {
        Assert::AreEqual(WRITE_NOT_IMPLEMENTED, e.what());
      }
    }

    TEST_METHOD(A_listening_tcp_socket_will_throw_a_runtime_error_when_trying_to_receive)
    {
      try
      {
        auto server = std::make_shared<SocketContext>();
        server->Initialise<TcpSocketClosedState, TcpSocketHandle>();
        server->Listen(1337);
        char buffer[TEST_BUFFER_SIZE];
        server->Read(buffer, TEST_BUFFER_SIZE);
        Assert::Fail(L"Runtime Error Expected");
      }
      catch (SocketException e)
      {
        Assert::AreEqual(READ_NOT_IMPLEMENTED, e.what());
      }
    }

    TEST_METHOD(A_failed_connection_attempt_will_throw_a_runtime_error)
    {
      try
      {
        auto client = std::make_shared<SocketContext>();
        client->Initialise<TcpSocketClosedState, TcpSocketHandle>();
        client->Open("localhost", TEST_SOCKET_PORT);
      }
      catch (SocketException e)
      {
        Assert::AreEqual("TcpSocketHandle::Connect() has failed - ErrorCode: 10061", e.what());
      }
    }

    TEST_METHOD(A_connected_client_and_server_can_pass_data_over_tcp)
    {
      RunServer();

      try
      {
        auto client = std::make_shared<SocketContext>();
        client->Initialise<TcpSocketClosedState, TcpSocketHandle>();
        client->Open("localhost", TEST_SOCKET_PORT);
        client->Write(TEST_MESSAGE, TEST_MESSAGE_SIZE);
      }
      catch(SocketException e)
      {
        server->Close();
        if (m_server.joinable())
          m_server.join();

        Assert::Fail(get_wchar(e.what()));
      }
      
      if(m_server.joinable())
        m_server.join();

      Assert::AreEqual(TEST_MESSAGE, m_receiveBuffer);
    }


    // Test helper methods
    void RunServer()
    {
      m_server = std::thread(&UnitTest1::ServerThread, this);
      std::this_thread::yield();
    }
    
    void ServerThread()
    {
      server = std::make_shared<SocketContext>();
      try
      {
        server->Initialise<TcpSocketClosedState, TcpSocketHandle>();

        server->Listen(TEST_SOCKET_PORT);
        auto client = server->Accept();
        memset(m_receiveBuffer, 0, TEST_BUFFER_SIZE);
        client->Read(m_receiveBuffer, TEST_BUFFER_SIZE);
        client->Close();
      }
      catch (SocketException e)
      {
        OutputDebugString(get_wchar(e.what()));
      }
    }

    const wchar_t *get_wchar(const char *c)
    {
      std::wostringstream wstr;
      wstr << c;
      return wstr.str().c_str();
    }
  };
}