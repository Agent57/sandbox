#include "TcpSocketHandle.h"
#include "SocketException.h"

#include <ws2tcpip.h>
#include <sstream>

TcpSocketHandle::TcpSocketHandle() : m_socket(INVALID_SOCKET)
{
  Initialise();
}

TcpSocketHandle::TcpSocketHandle(SOCKET handle) : m_socket(handle)
{
  Initialise();
}

TcpSocketHandle::~TcpSocketHandle()
{
  if (m_socket != INVALID_SOCKET)
    closesocket(m_socket);

  WSACleanup();
}

void TcpSocketHandle::Listen(int port)
{
  SetSocketInfo(nullptr, port);
  CreateSocket(m_info);

  if (bind(m_socket, m_info->ai_addr, int(m_info->ai_addrlen)) == SOCKET_ERROR)
    throw SocketException("TcpSocketHandle::Bind() has failed", WSAGetLastError());

  if (listen(m_socket, SOMAXCONN) == SOCKET_ERROR)
    throw SocketException("TcpSocketHandle::Listen() has failed", WSAGetLastError());
}

SocketHandlePtr TcpSocketHandle::Accept()
{
  auto client = accept(m_socket, nullptr, nullptr);
  if (client == INVALID_SOCKET)
    throw SocketException("TcpSocketHandle::Accept() has failed", WSAGetLastError());

  return std::make_unique<TcpSocketHandle>(client);
}

void TcpSocketHandle::Connect(const std::string& host, int port)
{
  auto error = 0;
  SetSocketInfo(host.c_str(), port);

  for (auto ptr = m_info; ptr != nullptr; ptr = ptr->ai_next)
  {
    CreateSocket(ptr);
    if (connect(m_socket, ptr->ai_addr, int(ptr->ai_addrlen)) == SOCKET_ERROR)
    {
      error = WSAGetLastError();
      closesocket(m_socket);
      continue;
    }
    return;
  }
  throw SocketException("TcpSocketHandle::Connect() has failed", error);
}

int TcpSocketHandle::Send(char* buffer, int length)
{
  auto bytesSent = send(m_socket, buffer, length, 0);
  if (bytesSent == SOCKET_ERROR)
    throw SocketException("TcpSocketHandle::Send() has failed", WSAGetLastError());

  return bytesSent;
}

int TcpSocketHandle::Receive(char* buffer, int length)
{
  auto bytesReceived = recv(m_socket, buffer, length, 0);
  if (bytesReceived == SOCKET_ERROR)
    throw SocketException("TcpSocketHandle::Receive() has failed", WSAGetLastError());

  return bytesReceived;
}

void TcpSocketHandle::Shutdown()
{
  if (m_socket == INVALID_SOCKET)
    return;

  if (shutdown(m_socket, SD_SEND) == SOCKET_ERROR)
    throw SocketException("TcpSocketHandle::Shutdown() has failed", WSAGetLastError());

  Close();
}

void TcpSocketHandle::Close()
{
  closesocket(m_socket);
  m_socket = INVALID_SOCKET;
}

// Private Methods
void TcpSocketHandle::Initialise()
{
  WSADATA wsaData;

  if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
    throw SocketException("TcpSocketHandle::Initialise() has failed", WSAGetLastError());
}

void TcpSocketHandle::SetSocketInfo(const char* const host, int port)
{
  struct addrinfo info;
  memset(&info, 0, sizeof(info));

  info.ai_family = AF_UNSPEC;
  info.ai_socktype = SOCK_STREAM;
  info.ai_protocol = IPPROTO_TCP;

  if (!host)
  {
    info.ai_family = AF_INET;
    info.ai_flags = AI_PASSIVE;
  }

  std::ostringstream strPort;
  strPort << port;

  auto result = getaddrinfo(host, strPort.str().c_str(), &info, &m_info);
  if (result != 0)
    throw SocketException("TcpSocketHandle::SetSocketInfo() has failed", result);
}

void TcpSocketHandle::CreateSocket(PADDRINFOA info)
{
  m_socket = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
  if (m_socket == INVALID_SOCKET)
    throw SocketException("TcpSocketHandle::CreateSocket() has failed", WSAGetLastError());
}