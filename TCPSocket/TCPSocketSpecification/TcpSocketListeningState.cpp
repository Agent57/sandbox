#include "TcpSocketListeningState.h"
#include "TcpSocketClosedState.h"
#include "TcpSocketOpenState.h"

TcpSocketListeningState::~TcpSocketListeningState()
{
  if (m_handle)
    m_handle->Close();
}

SocketContextPtr TcpSocketListeningState::Accept()
{
  auto handle = m_handle->Accept();
  auto newContext = std::make_shared<SocketContext>();
  newContext->SetState(std::make_unique<TcpSocketOpenState>(newContext.get(), move(handle)));
  return newContext;
}

void TcpSocketListeningState::Close()
{
  m_handle->Close();
  if(m_context)
    m_context->SetState(std::make_unique<TcpSocketClosedState>(m_context, move(m_handle)));
}
