#pragma once

#include <memory>
#include <string>

typedef std::unique_ptr<class ISocketHandle> SocketHandlePtr;

class ISocketHandle
{
public:
  virtual ~ISocketHandle()
  {    
  }

  virtual void Listen(int port) = 0;
  virtual SocketHandlePtr Accept() = 0;
  virtual void Connect(const std::string& host, int port) = 0;
  virtual int Send(char* buffer, int length) = 0;
  virtual int Receive(char* buffer, int length) = 0;
  virtual void Shutdown() = 0;
  virtual void Close() = 0;
};
