#pragma once

#include "SocketState.h"

class TcpSocketOpenState : public SocketState
{
public:
  explicit TcpSocketOpenState(SocketContext* context, SocketHandlePtr handle)
    : SocketState(context, move(handle))
  {    
  }

  virtual ~TcpSocketOpenState();

  virtual int Read(char* buffer, int length) override;
  virtual int Write(char* buffer, int length) override;
  virtual void Close() override;
};
