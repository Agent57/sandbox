#pragma once

#include "ISocketActions.h"

class SocketContext
{
  SocketActions m_state;

public:
  virtual ~SocketContext()
  {
    m_state->Close();
  }

  template <class state, class handle>
  void Initialise()
  {
    m_state = std::make_unique<state>(this, std::make_unique<handle>());
  }

  void Open(const std::string& host, int port);
  void Listen(int port);
  virtual SocketContextPtr Accept();
  int Read(char* buffer, int length);
  int Write(char* buffer, int length);
  void Close();
  void SetState(SocketActions state);
};
