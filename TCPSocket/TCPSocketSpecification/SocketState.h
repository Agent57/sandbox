#pragma once

#include "ISocketHandle.h"
#include "SocketContext.h"
#include "SocketException.h"

const char* const OPEN_NOT_IMPLEMENTED = "An override of SocketState::Open() has not been implemented";
const char* const LISTEN_NOT_IMPLEMENTED = "An override of SocketState::Listen() has not been implemented";
const char* const ACCEPT_NOT_IMPLEMENTED = "An override of SocketState::Accept() has not been implemented";
const char* const READ_NOT_IMPLEMENTED = "An override of SocketState::Read() has not been implemented";
const char* const WRITE_NOT_IMPLEMENTED = "An override of SocketState::Write() has not been implemented";
const char* const CLOSE_NOT_IMPLEMENTED = "An override of SocketState::Close() has not been implemented";

class SocketState : public ISocketActions
{
protected:
  SocketContext* m_context;
  SocketHandlePtr m_handle;

public:
  explicit SocketState(SocketContext* context, SocketHandlePtr handle)
    : m_context(context), m_handle(move(handle))
  {    
  }

  virtual void Open(const std::string& host, int port) override
  {
    throw SocketException(OPEN_NOT_IMPLEMENTED);
  }

  virtual void Listen(int port) override
  {
    throw SocketException(LISTEN_NOT_IMPLEMENTED);
  }
  
  virtual SocketContextPtr Accept() override
  {
    throw SocketException(ACCEPT_NOT_IMPLEMENTED);
  }
  
  virtual int Read(char* buffer, int length) override
  {
    throw SocketException(READ_NOT_IMPLEMENTED);
  }
  
  virtual int Write(char* buffer, int length) override
  {
    throw SocketException(WRITE_NOT_IMPLEMENTED);
  }
  
  virtual void Close() override
  {
    throw SocketException(CLOSE_NOT_IMPLEMENTED);
  }
};
