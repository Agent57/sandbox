#pragma once

#include "SocketState.h"


class TcpSocketClosedState : public SocketState
{
public:
  explicit TcpSocketClosedState(SocketContext* context, SocketHandlePtr handle)
    : SocketState(context, move(handle))
  {    
  }

  virtual void Close() override
  {
  }

  virtual void Open(const std::string& host, int port) override;
  virtual void Listen(int port) override;
};
