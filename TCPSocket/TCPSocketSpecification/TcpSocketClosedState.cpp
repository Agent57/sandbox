#include "TcpSocketClosedState.h"
#include "TcpSocketOpenState.h"
#include "TcpSocketListeningState.h"

void TcpSocketClosedState::Open(const std::string& host, int port)
{
  m_handle->Connect(host, port);

  if (m_context)
    m_context->SetState(std::make_unique<TcpSocketOpenState>(m_context, move(m_handle)));
}

void TcpSocketClosedState::Listen(int port)
{
  m_handle->Listen(port);

  if (m_context)
    m_context->SetState(std::make_unique<TcpSocketListeningState>(m_context, move(m_handle)));
}