#pragma once

#include <memory>
#include <string>

typedef std::shared_ptr<class SocketContext> SocketContextPtr;

class ISocketActions
{
public:
  virtual ~ISocketActions()
  {    
  }

  virtual void Open(const std::string& host, int port) = 0;
  virtual void Listen(int port) = 0;
  virtual SocketContextPtr Accept() = 0;
  virtual int Read(char* buffer, int length) = 0;
  virtual int Write(char* buffer, int length) = 0;
  virtual void Close() = 0;
};

typedef std::unique_ptr<ISocketActions> SocketActions;
