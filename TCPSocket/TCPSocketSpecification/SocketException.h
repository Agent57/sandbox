#pragma once

#include <sstream>

class SocketException : public std::exception
{
  std::string m_Message;

public:
  SocketException()
    : m_Message("")
  {    
  }

  explicit SocketException(char const* const pMessage)
    : m_Message(pMessage)
  {    
  }

  SocketException(char const* const pMessage, int code)
  {
    std::ostringstream ss;
    ss << pMessage << " - ErrorCode: " << code;
    m_Message = ss.str();
  }

  virtual char const* what() const override
  {
    return m_Message.c_str();
  }
};