namespace TicTacToe
{
  public class Combo
  {
    private Position _a;
    private Position _b;
    private Position _c;
    public int i { get { return (int)_a; } }
    public int j { get { return (int)_b; } }
    public int k { get { return (int)_c; } }

    public Combo(Position a, Position b, Position c)
    {
      _a = a;
      _b = b;
      _c = c;
    }
  }
}