﻿using NUnit.Framework;

namespace TicTacToe
{
  [TestFixture]
  public class TicTacToeSpecification
  {
    private Game _game;
    private const string FirstMove = "....X....";
    private const string FieldsAllTaken = "XOXXOXOXO";
    private const string WinningColumn = ".XO.X.OX.";
    private const string WinningRow = ".O.XXX.O.";
    private const string WinningDiagonal = "OX.XOX.XO";

    [SetUp]
    public void Setup()
    {
      _game = new Game();
    }

    [Test]
    public void a_game_is_over_when_all_fields_are_taken()
    {
      _game.Fields = FieldsAllTaken.ToCharArray();

      Assert.That(_game.Over(), Is.True);
    }

    [Test]
    public void a_game_is_over_when_all_fields_in_a_column_are_taken_by_a_player()
    {
      _game.Fields = WinningColumn.ToCharArray();
      
      Assert.That(_game.Over(), Is.True);
    }

    [Test]
    public void a_game_is_over_when_all_fields_in_a_row_are_taken_by_a_player()
    {
      _game.Fields = WinningRow.ToCharArray();

      Assert.That(_game.Over(), Is.True);
    }

    [Test]
    public void a_game_is_over_when_all_fields_in_a_diagonal_are_taken_by_a_player()
    {
      _game.Fields = WinningDiagonal.ToCharArray();

      Assert.That(_game.Over(), Is.True);
    }

    [Test]
    public void a_player_can_take_a_field_if_not_already_taken()
    {
      _game.SetField(Position.MiddleMiddle, 'X');

      Assert.That(_game.Fields, Is.EqualTo(FirstMove.ToCharArray()));
    }

    [Test]
    public void players_take_turns_taking_fields_until_the_game_is_over()
    {
      _game.SetField(Position.TopMiddle, 'X');
      _game.SetField(Position.TopRight, 'O');
      _game.SetField(Position.MiddleMiddle, 'X');
      _game.SetField(Position.BottomLeft, 'O');
      _game.SetField(Position.BottomMiddle, 'X');

      Assert.That(_game.Fields, Is.EqualTo(WinningColumn.ToCharArray()));
      Assert.That(_game.Over(), Is.True);
    }
  }
}
