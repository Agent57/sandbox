namespace TicTacToe
{
  public class Winner
  {
    public Combo[] Combo { get; private set; }

    public Winner()
    {
      Combo = new[]
      {
        new Combo (Position.TopLeft, Position.MiddleMiddle, Position.BottomRight),
        new Combo (Position.TopRight, Position.MiddleMiddle, Position.BottomLeft),
        new Combo (Position.TopLeft, Position.TopMiddle, Position.TopRight),
        new Combo (Position.MiddleLeft, Position.MiddleMiddle, Position.MiddleRight),
        new Combo (Position.BottomLeft, Position.BottomMiddle, Position.BottomRight),
        new Combo (Position.TopLeft, Position.MiddleLeft, Position.BottomLeft),
        new Combo (Position.TopMiddle, Position.MiddleMiddle, Position.BottomMiddle),
        new Combo (Position.TopRight, Position.MiddleRight, Position.BottomRight)
      };
    }
  }
}