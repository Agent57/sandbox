namespace TicTacToe
{
  public class Game
  {
    private readonly Winner _winner = new Winner();

    public char[] Fields { get; set; }

    public Game()
    {
      Fields = ".........".ToCharArray();
    }

    public bool Over()
    {
      foreach (var combo in _winner.Combo)
      {
        if(WinningCombo(combo.i, combo.j, combo.k))
          return true;
      }
      
      return AllFieldsTaken();
    }

    private bool WinningCombo(int i, int j, int k)
    {
      if (!Taken(Fields[i]))
        return false;

      return ((Fields[i] == Fields[j]) &&
              (Fields[i] == Fields[k]));
    }

    private bool AllFieldsTaken()
    {
      foreach (var field in Fields)
      {
        if (!Taken(field))
          return false;
      }
      return true;
    }

    private bool Taken(char field)
    {
      return field == 'X' || field == 'O';
    }

    public void SetField(Position position, char mark)
    {
      if (!Taken(Fields[(int) position]))
        Fields[(int) position] = mark;
    }
  }
}