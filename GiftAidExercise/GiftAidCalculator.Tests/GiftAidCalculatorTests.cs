﻿using Moq;
using NUnit.Framework;

namespace GiftAidCalculator.Tests
{
  [TestFixture]
  class GiftAidCalculatorTests
  {
    private Mock<IDataStore> _mockDataStore;
    private GiftAidCalculator _calculator;

    private const decimal TestTaxRate = 20.0m;
    private const decimal TestDonation = 5.264m;
    private const decimal GiftAidAmountOnTestDonation = 1.316m;
    private const string DisplayedGiftAidOnTestDonation = "1.32";
    private const decimal Donate100 = 100m;
    private const string DisplayedGiftAidOn100 = "25.00";
    private const decimal RunningEventGiftAidSupplement = 1.25m;
    private const decimal SwimmingEventGiftAidSupplement = 0.75m;
    private const decimal ZeroSupplement = 0.0m;

    [SetUp]
    public void Setup()
    {
      _mockDataStore = new Mock<IDataStore>();
      _mockDataStore.Setup(mock => mock.CurrentTaxRate).Returns(TestTaxRate);
      _calculator = new GiftAidCalculator(_mockDataStore.Object);
    }

    [Test]
    public void GiftAidAmount_WillCalculateGiftAidAtGivenTaxRate()
    {
      var giftAidAmount = _calculator.GiftAidAmount(TestDonation);

      Assert.AreEqual(GiftAidAmountOnTestDonation, giftAidAmount);
    }

    [Test]
    public void GiftAidDisplayValue_WillDisplayTheGiftAidAmountRoundedTo2DecimalPlaces()
    {
      var displayValue = _calculator.GiftAidDisplayValue(TestDonation);

      Assert.AreEqual(DisplayedGiftAidOnTestDonation, displayValue);
    }

    [Test]
    public void GiftAidDisplayValue_WillDisplayDecimalPlacesZeroFilled()
    {
      var displayValue = _calculator.GiftAidDisplayValue(Donate100);

      Assert.AreEqual(DisplayedGiftAidOn100, displayValue);
    }

    [Test]
    public void EventSupplement_WillCalculateAZeroPercentSupplementForUnspecifiedEventTypes()
    {
      var supplement = _calculator.EventSupplement(Donate100);

      Assert.AreEqual(ZeroSupplement, supplement);
    }

    [Test]
    public void EventSupplement_WillCalculateA5PercentSupplementForRunningEvents()
    {
      _calculator.Event = EventFactory.Create(EventType.Running);

      var supplement = _calculator.EventSupplement(Donate100);

      Assert.AreEqual(RunningEventGiftAidSupplement, supplement);
    }

    [Test]
    public void EventSupplement_WillCalculateA3PercentSupplementForSwimmingEvents()
    {
      _calculator.Event = EventFactory.Create(EventType.Swimming);

      var supplement = _calculator.EventSupplement(Donate100);

      Assert.AreEqual(SwimmingEventGiftAidSupplement, supplement);
    }
  }
}
