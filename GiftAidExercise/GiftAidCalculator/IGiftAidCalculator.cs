﻿namespace GiftAidCalculator
{
  public interface IGiftAidCalculator
  {
    IEvent Event { get; set; }
    decimal CurrentTaxRate { get; }
    decimal GiftAidAmount(decimal donationAmount);
    string GiftAidDisplayValue(decimal donationAmount);
    decimal EventSupplement(decimal donationAmount);
  }
}
