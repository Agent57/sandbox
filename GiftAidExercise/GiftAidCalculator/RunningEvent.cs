﻿namespace GiftAidCalculator
{
  public class RunningEvent : IEvent
  {
    public decimal Supplement(decimal amount)
    {
      return amount*0.05m;
    }
  }
}