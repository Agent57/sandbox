namespace GiftAidCalculator
{
  public class NullEvent : IEvent
  {
    public decimal Supplement(decimal amount)
    {
      return 0.0m;
    }
  }
}