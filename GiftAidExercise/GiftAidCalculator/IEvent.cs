﻿namespace GiftAidCalculator
{
  public interface IEvent
  {
    decimal Supplement(decimal amount);
  }
}