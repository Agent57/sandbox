﻿namespace GiftAidCalculator
{
  public class GiftAidCalculator : IGiftAidCalculator
  {
    private readonly IDataStore _dataStore;

    public IEvent Event { get; set; }

    public decimal CurrentTaxRate
    {
      get { return _dataStore.CurrentTaxRate; }
    }

    public GiftAidCalculator(IDataStore dataStore)
    {
      _dataStore = dataStore;
      Event = EventFactory.Create(EventType.None);
    }

    public decimal GiftAidAmount(decimal donationAmount)
    {
      var gaRatio = CurrentTaxRate / (100 - CurrentTaxRate);
      return donationAmount * gaRatio;
    }

    public string GiftAidDisplayValue(decimal donationAmount)
    {
      var amount = GiftAidAmount(donationAmount);
      return amount.ToString("0.00");
    }

    public decimal EventSupplement(decimal donationAmount)
    {
      return Event.Supplement(GiftAidAmount(donationAmount));
    }
  }
}
