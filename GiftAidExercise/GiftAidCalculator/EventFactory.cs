﻿namespace GiftAidCalculator
{
  public class EventFactory
  {
    public static IEvent Create(EventType type)
    {
      switch (type)
      {
        case EventType.Running:
          return new RunningEvent();

        case EventType.Swimming:
          return new SwimmingEvent();

        default:
          return new NullEvent();
      }
    }
  }
}