﻿namespace GiftAidCalculator
{
  public class SwimmingEvent : IEvent
  {
    public decimal Supplement(decimal amount)
    {
      return amount*0.03m;
    }
  }
}