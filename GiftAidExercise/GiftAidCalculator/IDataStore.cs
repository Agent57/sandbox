namespace GiftAidCalculator
{
  public interface IDataStore
  {
    decimal CurrentTaxRate { get; set; }
  }
}