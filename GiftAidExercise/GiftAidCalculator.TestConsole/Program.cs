﻿using System;

namespace GiftAidCalculator.TestConsole
{
	class Program
	{

	  static void Main(string[] args)
		{
      var presenter = new ConsolePresenter();

      Console.WriteLine("As Administrator: Please enter current tax rate:");
      presenter.SetCurrentTaxRate(presenter.ParseNumericInput());

      Console.WriteLine("As Promoter: Please select event type:");
      Console.WriteLine("  1: Running Event");
      Console.WriteLine("  2: Swimming Event");
      Console.WriteLine("  3: Other");
      presenter.SetEventType(presenter.ParseNumericInput());

      Console.WriteLine("As Doner: Please Enter donation amount:");
      var donation = presenter.ParseNumericInput();

      Console.WriteLine("Actual gift aid amount: " + presenter.GiftAidAmount(donation));
      Console.WriteLine("Gift aid display value: " + presenter.GiftAidDisplayValue(donation));
      Console.WriteLine("Gift aid event supplement: " + presenter.GiftAidEventSupplement(donation));
      Console.WriteLine("Press any key to exit.");
			Console.ReadLine();
		}
	}
}
