using System;
using Moq;

namespace GiftAidCalculator.TestConsole
{
  class ConsolePresenter
  {
    private IGiftAidCalculator _calculator;

    public decimal ParseNumericInput()
    {
      return decimal.Parse(Console.ReadLine());
    }

    public void SetCurrentTaxRate(decimal taxRate)
    {
      var mockDataStore = new Mock<IDataStore>();
      mockDataStore.Setup(mock => mock.CurrentTaxRate).Returns(taxRate);
      _calculator = new GiftAidCalculator(mockDataStore.Object);
    }

    public void SetEventType(decimal type)
    {
      _calculator.Event = EventFactory.Create((EventType)type);
    }

    public decimal GiftAidAmount(decimal donationAmount)
    {
      return _calculator.GiftAidAmount(donationAmount);
    }

    public string GiftAidDisplayValue(decimal donationAmount)
    {
      return _calculator.GiftAidDisplayValue(donationAmount);
    }

    public decimal GiftAidEventSupplement(decimal donationAmount)
    {
      return _calculator.EventSupplement(donationAmount);
    }
  }
}